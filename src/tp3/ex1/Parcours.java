package tp3.ex1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import tp3.ex1.collections.OrderedCollection;
import tp3.ex1.collections.Queue;
import tp3.ex1.collections.Stack;

public class Parcours {
	private static final boolean SLOW_MODE = false;
	
	private static final Random RNG = new Random();
	
	public static final void parcoursProfondeur() {
		parcoursThreaded(new Stack<Cellule>());
	}
	
	public static final void parcoursLargeur() {
		parcoursThreaded(new Queue<Cellule>());
	}
	
	public static final void parcoursThreaded(OrderedCollection<Cellule> chemin) {
		Thread t = new Thread(() -> {
			parcours(chemin);
		});
		t.start();
	}
	
	public static final void parcours(OrderedCollection<Cellule> chemin) {		
		final Labyrinthe lb = new Labyrinthe(chemin.getClass().getSimpleName());
		
		Cellule ici = new Cellule(0, 1);
		
		chemin.insert(ici);
		poserMarqueChemin(lb, ici);
		
		while (!chemin.isEmpty()) {
			modeRalenti();
			
			ici = chemin.peek();
			if (estSortie(lb, ici)) {
				Cellule trace = chemin.peek();
				
				while (trace.hasParent()) {
					poserMarqueChemin(lb, trace);
					trace = trace.getParent();
				}
				
				chemin.clear();
			} else {
				Cellule voisin = obtenirVoisin(lb, ici);
				if (voisin != null) {
					voisin.setParent(ici);
					
					chemin.insert(voisin);
					poserMarque(lb, voisin);
				} else {
					chemin.take();					
				}
			}
		}
	}
	
	// === UTIL ===
	
	private static void modeRalenti() {
		if (SLOW_MODE) {
			try {
				Thread.sleep(10);
			} catch(InterruptedException e) {}
		}
	}
	
	private static Cellule obtenirVoisin(Labyrinthe lb, Cellule c) {
		Cellule[] voisins = obtenirVoisins(lb, c);
		if (voisins.length == 0) {
			return null;
		}
		return voisins[RNG.nextInt(voisins.length)];
	}
	
	private static Cellule[] obtenirVoisins(Labyrinthe lb, Cellule c) {
		List<Cellule> voisins = new ArrayList<Cellule>();
		Cellule voisin;
		for (Cellule direction : Cellule.DIRECTIONS) {
			voisin = c.add(direction);
			if (existe(lb, voisin) && estLibre(lb, voisin)) {
				voisins.add(voisin);
			}
		}
		return voisins.toArray(Cellule[]::new);
	}
	
	
	private static boolean existe(Labyrinthe lb, Cellule c) {
		final int n = lb.n();
		return c.x >= 0 && c.x < n
			&& c.y >= 0 && c.y < n;
	}
	
	private static void poserMarque(Labyrinthe lb, Cellule c) {
		lb.poserMarque(c.x, c.y);
	}
	
	private static boolean estLibre(Labyrinthe lb, Cellule c) {
		return !estMur(lb, c) && !estMarque(lb, c);
	}
	
	private static boolean estMarque(Labyrinthe lb, Cellule c) {
		return lb.estMarque(c.x, c.y);
	}
	
	private static void poserMarqueChemin(Labyrinthe lb, Cellule c) {
		lb.poserMarqueChemin(c.x, c.y);
	}
	
	private static boolean estMur(Labyrinthe lb, Cellule c) {
		return lb.estMur(c.x, c.y);
	}
	
	private static boolean estSortie(Labyrinthe lb, Cellule c) {
		return lb.estSortie(c.x, c.y);
	}
}
