package tp3.ex1;

public class Cellule {
	public static final Cellule UP = new Cellule(0, 1);
	public static final Cellule RIGHT = new Cellule(1, 0);
	public static final Cellule DOWN = new Cellule(0, -1);
	public static final Cellule LEFT = new Cellule(-1, 0);

	public static final Cellule[] DIRECTIONS = new Cellule[] {
		UP, RIGHT, DOWN, LEFT
	};
	
	private Cellule parent;
	public final int x, y;
	
	public Cellule(int x, int y) {
		this.parent = null;
		this.x = x;
		this.y = y;
	}
	
	public boolean hasParent() {
		return parent != null;
	}
	
	public Cellule getParent() {
		return parent;
	}
	
	public void setParent(Cellule p) {
		parent = p;
	}
	
	public Cellule add(Cellule other) {
		return new Cellule(x + other.x, y + other.y);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Cellule other = (Cellule) obj;
		if (x != other.x) {
			return false;
		}
		if (y != other.y) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return String.format("[%d;%d]", x, y);
	}
}
