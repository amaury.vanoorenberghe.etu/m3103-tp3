package tp3.ex1;

import tp3.ex1.collections.Pile;

public class Main {
	public static void main(String[] args) {
		Parcours.parcoursProfondeur();
		Parcours.parcoursLargeur();
		
		Parcours.parcoursThreaded(new Pile<Cellule>());
	}
}
