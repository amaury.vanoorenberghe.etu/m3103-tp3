package tp3.ex1.collections;

import java.util.NoSuchElementException;

public class File<T> implements OrderedCollection<T> {
	private static int DEFAULT_CAPACITY = 64;
	private static float EXTEND_FACTOR = 1.5f;
	
	private T[] contenu;
	private int tete, queue;
	
	@SuppressWarnings("unchecked")
	public File() {
		contenu = (T[]) new Object[DEFAULT_CAPACITY];
		clear();
	}
	
	@Override
	public T peek() {
		return null;
	}

	@Override
	public void insert(T value) {
		
	}

	@Override
	public T take() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		
		
	}

	@Override
	public void clear() {
		tete = queue = 0;
	}

	@Override
	public boolean isEmpty() {
		return false;
	}
	
	private int dec(int value) {
		int result = value - 1;
		
		if (result == -1) {
			result = contenu.length - 1;
		}
		
		return result;
	}

}
