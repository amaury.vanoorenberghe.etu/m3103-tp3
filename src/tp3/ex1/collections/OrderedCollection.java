package tp3.ex1.collections;

public interface OrderedCollection<T> {
	public T peek();
	
	public void insert(T value);
	
	public T take();
	
	public void clear();
	
	public boolean isEmpty();
}
