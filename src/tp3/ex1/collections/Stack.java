package tp3.ex1.collections;

@SuppressWarnings("serial")
public class Stack<T> extends java.util.Stack<T> implements OrderedCollection<T> {
	public Stack() {
		super();
	}
	
	@Override
	public T peek() {
		return super.peek();
	}

	@Override
	public void insert(T value) {
		push(value);	
	}

	@Override
	public T take() {
		return pop();
	}

	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}
	
	@Override
	public void clear() {
		super.clear();
	}
}
