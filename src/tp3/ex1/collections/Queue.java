package tp3.ex1.collections;

import java.util.ArrayDeque;

@SuppressWarnings("serial")
public class Queue<T> extends ArrayDeque<T> implements OrderedCollection<T> {
	public Queue() {
		super();
	}
	
	@Override
	public T peek() {
		return super.peek();
	}

	@Override
	public void insert(T value) {
		offer(value);
	}

	@Override
	public T take() {
		// TODO Auto-generated method stub
		return poll();
	}

	@Override
	public boolean isEmpty() {
		return super.isEmpty();
	}
	
	@Override
	public void clear() {
		super.clear();
	}
}
