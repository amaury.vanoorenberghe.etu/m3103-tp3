package tp3.ex1.collections;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class Pile<T> implements OrderedCollection<T> {
	private static int DEFAULT_CAPACITY = 64;
	private static float EXTEND_FACTOR = 1.5f;
	
	private T[] contenu;
	private int sommet;
	
	@SuppressWarnings("unchecked")
	public Pile() {
		contenu = (T[]) new Object[DEFAULT_CAPACITY];
		sommet = -1;
	}
	
	@Override
	public T peek() {
		return contenu[sommet];
	}

	@Override
	public void insert(T value) {
		expandIfRequired();
		contenu[++sommet] = value;
	}

	@Override
	public T take() {
		if (isEmpty()) {
			throw new NoSuchElementException();
		}
		
		return contenu[sommet--];
	}

	@Override
	public void clear() {
		sommet = -1;
	}

	@Override
	public boolean isEmpty() {
		return sommet == -1;
	}
	
	private boolean isFull() {
		return sommet == contenu.length - 1;
	}
	
	private void expandIfRequired() {
		if (isFull()) {
			expand();
		}
	}
	
	private void expand() {
		contenu = Arrays.copyOf(contenu, (int)(contenu.length * EXTEND_FACTOR));
	}
}
